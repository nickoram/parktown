package com.amazon;

/**
 * Assumptions
 * 1 - ParkingGarage.getSpaces() returns an iterator on a Priority Queue
 *     - Priority Queue is sorted according to decreasing desirability
 * 2 - We are not allowed to modify interface methods for Car, Space,
 *     ParkingGarage, or GarageStatusListener. Therefore, we
 *     assume the ParkingGarage implementation will take care of
 *     modifying state of Space and Car objects
 * 3 - A car will find another space if their assigned
 *     space has been taken by another driver.
 * 4 - When a car exits the garage, ParkingGarage takes care of adding
 *     the newly made available space back into the Collection of all
 *     spaces
 */

import java.util.*;

/**
 * The SpaceAssigner is responsible for assigning a space for an incoming
 * car to park in. This is done by calling the assignSpace() API.
 *
 * The SpaceAssigner responds to changes in space availability by
 * implementing the GarageStatusListener interface.
 */
public class SpaceAssigner implements GarageStatusListener
{
    private static final int BATCH = 40;

    private ParkingGarage mParkingGarage;
    private PriorityQueue<Space> mAvailableSpaces;
    private Map<Car, Space> mAssignedSpaces;

    /**
     * Initiates the SpaceAssigner. This method is called only once per
     * app start-up.
     * @param garage The parking garage for which you are vending spaces.
     *
     * runtime: O(1) - constant time
     *               we are limiting size or priority queue to BATCH
     *               when there are no more available spaces, getBatch()
     *               will be called to increase size of collection
     *
     * storage: constant storage, only adding 40 Spaces to priority queue
     */
    public void initialize(ParkingGarage garage)
    {
        mParkingGarage = garage;

        mAvailableSpaces = new PriorityQueue<Space>(BATCH, new SpaceComparator());

        mAssignedSpaces = new HashMap<Car, Space>();

        // register the listener
        mParkingGarage.register(this);

        // populate available spaces
        if (!getBatch()) {
            throw new IllegalStateException("No Spaces available in garage");
        }
    }

    /**
     * assignSpace
     *
     * event listener method which assigns a space to an incoming car and
     * returns that space.
     *
     * @param car The incoming car that needs a space.
     * @returns The space reserved for the incoming car.
     *
     * runtime: O(1) retrieval from mAvailableSpaces because it is a
     *          priority queue. Most desirable space is the first element
     *          O(1) insertion into mAssignedSpaces because it is a
     *          HashMap. getBatch will incur insertions into priority
     *          queue, but still at a constant T(log(BATCH)) = O(1)
     *
     * storage: if mAvailableSpaces is empty, we will add BATCH number of
     *          spaces to priority queue. Footprint on mAssignedSpaces
     *          will be size of {car,space} when pair is inserted
     */
    public Space assignSpace(Car car)
    {
        if (mAvailableSpaces.isEmpty()) {
            if (!getBatch()) {
                // no available spaces
                return null;
            }
        }

        Iterator<Space> iterator = mAvailableSpaces.iterator();

        if (iterator.hasNext()) {
            Space space = iterator.next();
            mAssignedSpaces.put(car, space);
            return space;
        }

        return null;
    }

    /**
     * onSpaceTaken
     *
     * event listener method written to handle case when drivers take
     * incorrect space. Assume that a car will find another space if
     * their assigned space has been taken by another driver.
     *
     * runtime: mAssignedSpaces.remove(car) = O(1)
     *
     *          average case: is going to take O(1)
     *
     *          worst case: triggered only when car parks in unassigned
     *          space.
     *
     *          mAvailableSpaces.contains(space) = O(log(n))
     *          mAvailableSpaces.remove(space) = O(n)
     *
     *          really worst case: assigned space isn't occupied
     *          assigedSpace.isOccupied() = O(1)
     *          mAvailableSpaces.add(assigedSpace) = O(log(n)), where n
     *          is the number of spaces in mAvailableSpaces.
     *
     * storage: best case:
     *            mAvailableSpaces decreases in size by Space
     *            mAssignedSpaces decreased by size of {Car, Space}
     *
     *          average case:
     *            mAssignedSpaces decreased by size of {Car, Space}
     *            mAvailableSpaces stays the same
     *
     *          worst case:
     *            mAssignedSpaces decreased by size of {Car, Space}
     *            mAvailableSpaces increases by size of Space
     */
    public void onSpaceTaken(Car car, Space space)
    {
        // check to see if assigned space is space taken
        Space assigedSpace = mAssignedSpaces.remove(car);

        // only care about case where driver takes wrong space and
        // assigned space is not occupied
        if (!assigedSpace.equals(space)) {
            if (mAvailableSpaces.contains(space)) {
                mAvailableSpaces.remove(space);
            }

            if (!assigedSpace.isOccupied()) {
                mAvailableSpaces.add(assigedSpace);
            }
        }
    }

    /**
     * onSpaceFreed
     *
     * Redundant method, event listener method when car leaves space.
     * Leave space assigned to car until car exits garage. No action
     * taken
     *
     * runtime: N/A
     * storage: N/A
     */
    public void onSpaceFreed(Car car, Space space) {}

    /**
     * onGarageExit
     *
     * Redundant method, event listener method called when car exits
     * garage. Assume that ParkingGarage will add car's assigned space
     * back to collection of all available spaces
     *
     * runtime: N/A
     * storage: N/A
     */
    public void onGarageExit(Car car) {}

    /**
     * getBatch
     *
     * private method used to populate available spaces priority queue.
     * since we cannot load the entire index of spaces due to limited
     * memory and time constraints, we grow the number of spaces in the
     * priority queue in multiples of BATCH
     *
     * @return boolean true is there are any available spaces
     */
    private boolean getBatch() {

        Iterator<Space> spaceIterator = mParkingGarage.getSpaces();

        // initialize with BATCH Spaces since we cannot iterate
        for (int i=0; i < BATCH && spaceIterator.hasNext(); i++) {
            mAvailableSpaces.add(spaceIterator.next());
            spaceIterator.remove();
        }

        return !mAvailableSpaces.isEmpty();
    }
}
