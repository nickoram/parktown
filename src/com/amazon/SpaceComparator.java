package com.amazon;

import java.util.Comparator;

public class SpaceComparator implements Comparator<Space> {

    @Override
    public int compare(Space p, Space q) {

        int pWeight = p.getDesirability();
        int qWeight = q.getDesirability();

        // tie
        if (pWeight == qWeight) {
            return 0;
        } else if (pWeight > qWeight) {
            // pWeight is more desirable
            return -1;
        } else {
            // qWeight is more desirable
            return 1;
        }
    }
}
