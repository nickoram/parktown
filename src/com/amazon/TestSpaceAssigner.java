/**
 * Testing not quite complete due to running out of time
 * - future improvements
 * - test multiple cars entering garage parking in wrong spots
 * - test many cars entering garage, but no car actually parks
 */

package com.amazon;

import org.junit.Test;

import java.util.Iterator;
import java.util.PriorityQueue;

import static org.junit.Assert.assertTrue;

public class TestSpaceAssigner {

    @Test
    public void testAssignSpace() {
        System.out.println("testAssignSpace");

        TestParkingGarage testParkingGarage = new TestParkingGarage();
        SpaceAssigner testSpaceAssigner = new SpaceAssigner();
        testSpaceAssigner.initialize(testParkingGarage);
        Car testCar = testParkingGarage.getCar();
        Space space = testSpaceAssigner.assignSpace(testCar);

        assertTrue(space != null);
    }

    @Test
    public void testOnSpaceTaken() {
        System.out.println("testOnSpaceTaken");

        TestParkingGarage testParkingGarage = new TestParkingGarage();
        SpaceAssigner testSpaceAssigner = new SpaceAssigner();
        testSpaceAssigner.initialize(testParkingGarage);
        Car testCar = testParkingGarage.getCar();
        Space space = testSpaceAssigner.assignSpace(testCar);

        testParkingGarage.parkCar(testCar, space);
        testParkingGarage.exitGarage(testCar);
        assertTrue(space != null);
    }

    @Test
    public void onSpaceFreed() {
        System.out.println("onSpaceFreed");
    }

    @Test
    public void onGarageExit() {
        System.out.println("onGarageExit");
    }

    private class TestCar implements Car {
        @Override
        public String getLicensePlateState() {
            return null;
        }

        @Override
        public String getLicensePlateNumber() {
            return null;
        }
    }

    private class TestSpace implements Space {

        private int mId;
        private int mWeight;
        private boolean mOccupied;

        public TestSpace(int id, int weight) {
            setId(id);
            setWeight(weight);
            setOccupied(false);
        }

        @Override
        public int getID() {
            return mId;
        }

        @Override
        public int getDesirability() {
            return mWeight;
        }

        @Override
        public boolean isOccupied() {
            return mOccupied;
        }

        @Override
        public Car getOccupyingCar() {
            return null;
        }

        public void setId(int id) {
            mId = id;
        }

        public void setWeight(int weight) {
            mWeight = weight;
        }

        public void setOccupied(boolean occupied) {
            mOccupied = occupied;
        }

    }

    private class TestParkingGarage implements ParkingGarage {

        private static final int LIMIT = 120;

        private PriorityQueue<Space> mSpaces;
        private GarageStatusListener mGarageStatusListener;

        public TestParkingGarage() {
            mSpaces = new PriorityQueue<Space>(LIMIT, new SpaceComparator());

            for (int i=0; i < LIMIT; i++) {
                mSpaces.add(new TestSpace(i, (int) (Math.random() * LIMIT)));
            }
        }

        @Override
        public void register(GarageStatusListener assigner) {
            mGarageStatusListener = assigner;
        }

        @Override
        public Iterator<Space> getSpaces() {
            return mSpaces.iterator();
        }

        public Car getCar() {
            return new TestCar();
        }


        public void exitGarage(Car car) {
            mGarageStatusListener.onGarageExit(car);

        }

        public void parkCar(Car car, Space space) {
            mGarageStatusListener.onSpaceTaken(car, space);

        }
    }
}
